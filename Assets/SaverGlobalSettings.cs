﻿namespace Saver
{
    public static class SaverGlobalSettings
    {
        const string StandaloneWorkPath = "/Roaming/#Hacker.EnglishWords";
        const string AndroidWorkPath = "/programs/com.hacker.englishwords";
        const string IOSWorkPath = "/Hacker";

        public const string WordsFileName = "Words.txt";
        public const string SettingsFileName = "Settings.txt";

        public static string GetBaseFolder()
        {
            var dataPath = UnityEngine.Application.persistentDataPath;

            byte cutFolders = 0;
            string workPath = "";

            #region PlatformSpecific
            #if (UNITY_STANDALONE_WIN || UNITY_EDITOR)
                cutFolders = 3; workPath = StandaloneWorkPath;
            #endif

            #if UNITY_ANDROID && !UNITY_EDITOR
                cutFolders = 4; workPath = AndroidWorkPath;
            #endif

            #if UNITY_IOS && !UNITY_EDITOR
                cutFolders = 0; workPath = IOSWorkPath;
            #endif
            #endregion

            #region CutPath
            for (int i = 0; i < cutFolders; i++) dataPath = dataPath.Remove(GetIndexOfEnd('/', dataPath));
            #endregion

            dataPath += workPath;
            if (!System.IO.Directory.Exists(dataPath))
                System.IO.Directory.CreateDirectory(dataPath);

            return dataPath;
        }

        static int GetIndexOfEnd(char c, string Sourse)
        {
            if (!string.IsNullOrEmpty(Sourse))
                for (int i = Sourse.Length - 1; i > -1; i--)
                    if (Sourse[i] == c)
                        return i;
            return -1;
        }
    }
}
