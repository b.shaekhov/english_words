﻿using UnityEngine;

public static class CardsStorage{

    private static Card[] _cards;
    public static int CardsCount { get { if (_cards != null) return _cards.Length; return -1; } }
    
	public static void Init()
    {
        _cards = DataReader.GetAllCardsInFile(Saver.SaverGlobalSettings.GetBaseFolder()+"/" + Saver.SaverGlobalSettings.WordsFileName);
    }

    public static Card GetCard(int index)
    {
        if (index < 0 || index > _cards.Length)
        {
            Debug.LogError("ArgumentOutOfRangeException " + index + " " + _cards.Length);
            return null;
        }

        return _cards[index];
    }
}
