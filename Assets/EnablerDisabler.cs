﻿using UnityEngine;

public class EnablerDisabler : MonoBehaviour {

    public GameObject Target;

	public void ChangeState()
    {
        Target.SetActive(!Target.activeSelf);
    }
}
