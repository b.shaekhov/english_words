﻿using System.IO;

public static class DataReader {

    private const string ValidSymbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя'_-(){}:;?.,*!@~+=-[]<>%$/";
    private static string _fastSeparateSymbols = " ";

    static DataReader()
    {
        _fastSeparateSymbols += System.Environment.NewLine;
    }
    
    public static Card[] GetAllCardsInFile(string filePath)
    {
        if (!File.Exists(filePath))
        {
            File.Create(filePath).Close();
        }

        int count;
        var queue = DecodeRawData(File.ReadAllText(filePath), out count);

        var ans = new Card[count>>1];

        int counter = 0;
        var el = queue;

        bool eW = true;

        StringQueueElement eWVal = null;
        StringQueueElement tWVal = null;

        while (el != null)
        {
            if (eW)
            {
                eWVal = el;
            } else {
                tWVal = el;

                ans[counter] = new Card()
                {
                    OriginalWord = eWVal.value,
                    TranslateWord = tWVal.value
                };

                counter++;
            }

            eW = !eW;
            el = el.Next;
        }

        return ans;
    }
        
    static StringQueueElement DecodeRawData(string rawData, out int count)
    {
        StringQueueElement queue = null;
        StringQueueElement ans = null;

        count = 0;

        var curValue = new System.Text.StringBuilder();

        bool unloaded = true;

        for (int i = 0; i < rawData.Length; i++)
        {
            var sSymbol = rawData[i].ToString();

            bool LastSymbol = i == rawData.Length - 1;

            if (LastSymbol || _fastSeparateSymbols.Contains(sSymbol) || !ValidSymbols.Contains(sSymbol))
            {
                if (LastSymbol && ValidSymbols.Contains(sSymbol))
                {
                    curValue.Append(sSymbol);
                    unloaded = false;
                }

                if (!unloaded)
                {
                    unloaded = true;

                    if (queue == null)
                    {
                        queue = new StringQueueElement();
                        ans = queue;
                    } else
                    {
                        queue.Next = new StringQueueElement();
                        queue = queue.Next;
                    }

                    queue.value = curValue.ToString();
                    count++;

                    curValue.Remove(0, curValue.Length);
                }
            } else
            {
                curValue.Append(sSymbol);
                unloaded = false;
            }
        }

        return ans;
    }

    class StringQueueElement
    {
        public StringQueueElement Next = null;
        public string value;
    }
}
