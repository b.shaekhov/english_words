﻿using UnityEngine;
using UnityEngine.UI;

public class CardsChanger : MonoBehaviour {

    public int pos = 0;

    public Text OriginalLabel;
    public Text TranslateLabel;

    private void Start()
    {
        DrawCard(pos);
    }

    public void NextCard()
    {
        pos++;
        if (pos >= CardsStorage.CardsCount)
        {
            pos = 0;
        }
        DrawCard(pos);
    }

    public void PrevCard()
    {
        pos--;
        if (pos < 0)
        {
            pos = CardsStorage.CardsCount-1;
        }
        DrawCard(pos);
    }

    void DrawCard(int index)
    {
        if (index < CardsStorage.CardsCount)
        {
            var card = CardsStorage.GetCard(index);

            OriginalLabel.text = card.OriginalWord;
            TranslateLabel.text = card.TranslateWord;
        }
    }
}
